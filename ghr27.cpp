#include <algorithm>
#include <iostream>

using namespace std;

int Plus(int a, int b){
    return a + b;
}


void show(int array[], int n)
{
    for(int i = 0; i < n; i = i + 1)
    {
        cout << array[i] << " ";
    }
    cout << endl;
}

float M(int array[], int n)
{
    sort(array + 0, array + n);
    if(n % 2 != 0)
    { 
       return array[(n - 1) / 2];
    }
    else
    {
        return (array[n / 2] + array[(n / 2 - 1)]) / 2.0;
     
    }

}


int main()
{
    cout << "Plus(2, 3) = "
         << Plus(2, 3)
         << endl;
    
    int a[] = {3, 6, 2, 1, 9, 7};
    
    // cout << a[1];     

    cout << "1 + 2="
         <<  1 + 2
         << endl;
         
    show(a, 5);
    sort(a + 2, a + 5,greater<int>());
    show(a, 5);
    
    cout << M(a, 6) 
         << endl;
    // -> 3 
    
 	return 0;
	
}