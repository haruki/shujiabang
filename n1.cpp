#include <iostream>

using namespace std;

bool isPrime(int num){
    for(int i = 2; i < num; i = i + 1){
        if (num % i == 0){
            return false;
        }
    }
    return true;
}

int main()
{
    int n = 100; // n为输入的偶数
    
    // 输出所有组合
    for(int i = 2; i <= n / 2; i = i + 1){
        if (isPrime(i)){
            if (isPrime(n - i)){
                cout << i 
                     << " "
                     << n - i
                     << endl;
            }
        }
    }
    
	return 0;
}