print(2 * 3)
# -> 6


add1 = lambda x : x + 1
print("add1(1) =", add1(1))
# -> 2

sub1 = lambda x : x - 1
print("sub1(1) =", sub1(1))
# -> 0


square = lambda x : x * x
print("square(2) =", square(2))
# -> 4


# 函数作为另一个函数的输入
apply = lambda f, x : f(x)
print("apply(add1, 2) =", apply(add1, 2))
# -> 3


# 函数作为另一个函数的输出
f = lambda x : lambda y : x + y
print("f(2)(3)=",f(2)(3))
# -> 5


# 函数作为另一个函数的输入和输出
compose = lambda f, g : lambda x : f(g(x))
print("compose(add1, square)(3)=", compose(add1, square)(3))
# -> 5

print(add1(square(2)))
# -> 5

pi = 3.14
area = lambda r : pi * r * r
print("area(4)=", area(4))
# -> 16 * 3.14


sumOfSquare = lambda h, d : square(h) + square(d)
print("sumOfSquare(2, 3)=", sumOfSquare(2, 3))
# -> 13


three = lambda f : lambda x : f(f(f(x)))
print("three(add1)(2)=", three(add1)(2))
# -> 5

print("three(square)(2)=", three(square)(2))
# -> 256


one = lambda f : lambda x : f(x)
print("one(add1)(2)=", one(add1)(2))
# -> 3


zore = lambda f : lambda x : x

print("zore(square)(2)=", zore(square)(2))
# -> 2
print("zore(add1)(2)=", zore(add1)(2))
# -> 2


# 练习
# 1. 画出compose 和 two的计算图
# 2. 定义并测试函数succ，它能做到下面的事情

# three = succ(two)
# print(three(add1)(0))
# -> 3











