def sign(n):
    if n > 0:
        return 1
    elif n == 0:
        return 0
    else:
        return -1


print(sign(26))
#  -> 1

def sum(n) :
    if n == 0 :
        return 0
    else :
        return n + sum(n - 1)

print("sum(0)=", sum(0))
# -> 0


print("sum(5)=", sum(5))
# -> 15


def fact(n) :
    if n == 0 :
        return 1
    else :
        return n * (n - 1)


print("fact(0)=", fact(0))
# -> 1

print("fact(6)=", fact(6))
# -> 720


def fib(n) :
    if n == 0 :
        return 0
    elif n == 1 :
        return 1
    else :
        return fib(n - 1) + fib(n - 2)


print("fib(1)=", fib(1))
# -> 1

print("fib(10)=", fib(10))
# -> 55

# 练习
# 画出fib(5)的树型递归图
# plus