#  add1
def toNumber(n) :
    return n(add1)(0)


print("toNumber(three) =", toNumber(three))
# -> 3
    
print("toNumber(one) =", toNumber(one))
# -> 1


# 比较表达式, >, ==, <, !=
print(40 == 0)


# 函数定义
def sign(n):
    if n > 0:
        return 1
    else:
        if n == 0:
            return 0
        else:
            return -1

print("sign(23) =", sign(23))
# -> 1

print("sign(0) =", sign(0))
# -> 0 

print("sign(-9) =", sign(-9))
# -> -1


def isEven(n):
    return n % 2 == 0
    
print("isEven(6)=", isEven(6))
# -> True

print("isEven(3)=", isEven(3))
# -> False


# 练习
# 1. 写出toNumber(three)的单步替换
# 2. 写出compose(lambda x: x + 1, lambda x: x * x)的单步替换