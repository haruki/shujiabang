def plus(a,b):
    if b == 0:
        return a
    else:
        return add1(plus(a , sub1(b)))


print("plus(2,3) =", plus(2, 3))
print("plus(5,0) =", plus(5, 0))
print("plus(4,7) =", plus(4, 7))
print("plus(60,1) =", plus(60, 1))



def times(a, b):
    if b == 0 :
        return 0
    else:
        return plus(times(a, sub1(b)), a)


print("times(4, 6)=", times(4, 6))
print("times(2, 5)=", times(2, 5))
print("times(3, 9)=", times(3, 9))


def pow(a, b):
    if b == 0:
        return 1
    else:
        return times(pow(a, sub1(b)), a)


print("pow(2, 4)=", pow(2, 4))
print("pow(5, 2)=", pow(5, 2))


def pom(a, b):
    if b == 0:
        return 1
    else:
        return pow(a, pom(a, sub1(b)))


print("pom(3, 0)=", pow(3, 0))
print("pom(3, 3)=", pow(3, 3))
print("pom(2, 2)=", pom(2, 2))

def pomm(a, b):
    if b == 0:
        return 1
    else:
        return pom(a, pomm(a, sub1(b)))

print("pomm(2, 2)=", pomm(2, 2))
# print("pomm(2, 3)=", pomm(2, 3))


# r =(lambda x : x(x))(lambda x : x(x))
# print(r)

def plus1(a,b):
    if b == 0:
        return a
    else:
        return plus(add1(a), sub1(b))

print("plus1(2, 3)=", plus1(2, 3))
print("plus1(5,0) =", plus1(5, 0))
print("plus1(4,7) =", plus1(4, 7))


for i in [1, 2, 3, 4]:
    print(i)


print([2, 3][0])
print([2, 3][1])
print([2, 3, 4][2])

# 练习
# 1. Ackerman函数
# 2. 画出plus(2, 3)和plus1(2, 3)的替换，观察不同